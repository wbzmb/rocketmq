<?php
namespace RocketMQ\logger\core;

use RocketMQ\logger\bean\ILoggingEvent;

interface ClassicConverter{

    function convert(ILoggingEvent $ILoggingEvent);
}