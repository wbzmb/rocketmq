<?php
namespace RocketMQ\logger\driver\flume;

use RocketMQ\logger\bean\ILoggingEvent;
use RocketMQ\logger\core\ClassicConverter;

class JsonConverter implements ClassicConverter {

    public function convert(ILoggingEvent $iLoggingEvent) {
        return $iLoggingEvent->getFormattedMessage();
    }
}