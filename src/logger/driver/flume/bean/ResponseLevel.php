<?php
namespace RocketMQ\logger\driver\flume\bean;

class ResponseLevel {

    public static $INFO = "INFO";

    public static $ERROR = "ERROR";
}
