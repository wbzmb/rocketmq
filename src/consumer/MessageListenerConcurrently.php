<?php
namespace RocketMQ\consumer;

use RocketMQ\consumer\listener\ConsumeConcurrentlyContext;
use RocketMQ\consumer\listener\ConsumeConcurrentlyStatus;
use RocketMQ\entity\MessageExt;

interface MessageListenerConcurrently extends MessageListener
{
    /**
     * @param MessageExt[] $msgs
     * @param ConsumeConcurrentlyContext $context
     * @return string
     */
    function consumeMessage(array $msgs, ConsumeConcurrentlyContext $context);
}
