<?php
namespace RocketMQ\consumer;

use RocketMQ\consumer\listener\ConsumeOrderlyContext;
use RocketMQ\consumer\listener\ConsumeOrderlyStatus;
use RocketMQ\entity\MessageExt;

interface MessageListenerOrderly extends MessageListener
{
    /**
     * @param MessageExt[] $msgs
     * @param ConsumeOrderlyContext $context
     * @return ConsumeOrderlyStatus
     */
    function consumeMessage(array $msgs, ConsumeOrderlyContext $context);
}