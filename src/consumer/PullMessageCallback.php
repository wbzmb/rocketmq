<?php
namespace RocketMQ\consumer;

use Closure;
use RocketMQ\remoting\callback\InvokeCallback;
use RocketMQ\remoting\RemotingCommand;

class PullMessageCallback implements InvokeCallback
{
    /**
     * @var Closure
     */
    private $pullback;

    public function __construct(Closure $pullback){
        $this->pullback = $pullback;
    }

    function operationComplete(RemotingCommand $response){
        $pullback = $this->pullback;
        $pullback($response);
    }
}