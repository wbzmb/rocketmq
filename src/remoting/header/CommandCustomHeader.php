<?php
namespace RocketMQ\remoting\header;

interface CommandCustomHeader
{
    /**
     * @return array
     */
    function getHeader();
}