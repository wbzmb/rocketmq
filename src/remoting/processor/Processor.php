<?php
namespace RocketMQ\remoting\processor;

use RocketMQ\remoting\AbstractRemotingClient;
use RocketMQ\remoting\RemotingCommand;

interface Processor
{
    function execute(AbstractRemotingClient $client , RemotingCommand $remotingCommand);

    function exception(\Exception $e);
}