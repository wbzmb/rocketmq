<?php


namespace RocketMQ\remoting\heartbeat;


class ConsumeType
{
    const CONSUME_ACTIVELY = "PULL";

    const CONSUME_PASSIVELY = "PUSH";
}