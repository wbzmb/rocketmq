<?php


namespace RocketMQ\remoting\heartbeat;


class MessageModel
{
    const BROADCASTING = "BROADCASTING";

    const CLUSTERING = "CLUSTERING";
}