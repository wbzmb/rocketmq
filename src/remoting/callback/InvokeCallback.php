<?php
namespace RocketMQ\remoting\callback;

use RocketMQ\remoting\AbstractRemotingClient;
use RocketMQ\remoting\RemotingCommand;

interface InvokeCallback
{
    function operationComplete(RemotingCommand $remotingCommand);
}