<?php
namespace RocketMQ;

use RocketMQ\entity\TopicRouteData;
use RocketMQ\remoting\header\namesrv\GetRouteInfoRequestHeader;
use RocketMQ\remoting\NettyClient;
use RocketMQ\remoting\MessageDecoder;
use RocketMQ\remoting\MessageEncoder;
use RocketMQ\remoting\RemotingCommand;
use RocketMQ\remoting\RequestCode;
use RocketMQ\util\MQClientUtil;

class MQClient
{
    /**
     * @var NettyClient
     */
    private $nettyClient;

    public function __construct(){
        $this->nettyClient = new NettyClient();
    }


    /**
     * @param RemotingCommand $requestCommand
     * @return RemotingCommand
     * @throws exception\RocketMQClientException
     */
    public function sendMessage(RemotingCommand $requestCommand){
        $byteBuf = MessageEncoder::encode($requestCommand);
        $result = $this->nettyClient->send($byteBuf);
        $remotingCommand = MessageDecoder::decode($result);
        return $remotingCommand;
    }

    public function connect($addr){
        $this->nettyClient->connect($addr , 10);
    }

    public function shutdown(){
        $this->nettyClient->shutdown();
    }
}