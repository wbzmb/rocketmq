<?php
namespace RocketMQ\entity;

class GroupType
{
    const PRODUCER = "PRODUCER";

    const CONSUMER = "CONSUMER";
}