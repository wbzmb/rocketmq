<?php
namespace RocketMQ\entity;

class CommunicationMode
{
    const SYNC = "SYNC";

    const ASYNC = "ASYNC";

    const ONEWAY = "ONEWAY";
}