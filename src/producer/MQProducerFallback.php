<?php
namespace RocketMQ\producer;

use RocketMQ\entity\Message;
use RocketMQ\entity\SendResult;

interface MQProducerFallback
{
    function beforeSendMessage(Message $message);

    function afterSendMessage(Message $message , SendResult $sendResult);
}