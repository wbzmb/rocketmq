<?php
namespace RocketMQ\producer;

use RocketMQ\entity\Message;
use RocketMQ\entity\MessageExt;
use RocketMQ\entity\SendResult;

interface TransactionListener
{

    /**
     * 执行本地事务
     * @param Message $msg
     * @param SendResult $sendResult
     * @param $arg
     * @return mixed
     */
    function executeLocalTransaction(Message $msg , SendResult $sendResult, $arg);

    /**
     * 回查事务状态
     * @param MessageExt $msg
     * @return LocalTransactionState
     */
    function checkLocalTransaction(MessageExt $msg);
}